# DeployMesh

Unreal Engine Deploy Mesh to Handheld AR

<div align="center">
    ![Action Screenshot](readme/screenshot-deploymesh-scripted_actions-01.png)
</div>

## About

This repo includes Unreal Engine tools to automatically merge and deploy selected static mesh assets to the Unreal Engine Handheld AR workflow.

## Background

This Unreal Engine repo is part of my proof-of-concept pipeline & tools workflow for automation of the Unreal Engine course [Build a Detective's Office Game Environment](https://dev.epicgames.com/community/learning/courses/WK/unreal-engine-build-a-detective-s-office-game-environment/YOr/unreal-engine-introduction-to-the-detective-s-office-course).

I wanted to prototype tbe environment in AR as I iterated on the design of the props and structure. Useful resources for this task:

- [Unreal Engine Handheld Augmented Reality](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/XRDevelopment/AR/ARHowTos/HandheldAR/)
- [Apple ARKit](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/XRDevelopment/AR/ARPlatforms/ARKit/)
- [Distributing your app to registered devices](https://developer.apple.com/documentation/xcode/distributing-your-app-to-registered-devices)

## Requirements

- [Unreal Engine 4.27+](https://www.unrealengine.com/en-US)
- ARKit,ARCore-ready mobile device

## Setup

We can simply use the Handheld AR project and replace the included static mesh with our own.

### Update BP_Placeable blueprint

- Create a new project using the Handheld AR proejct template.
- Create a placeholder static mesh in the desired directory.  
- In the blueprint *BP_Placeable* > *Assign Product Asset* function, change the *BP_ProductAssetRef* variable to use the placeholder mesh.
- Optional: You may wish to add a *Scale Divisor* (see blueprint) to scale down the mesh for easier manipulation on a mobile device.

| ![BP_Placeable Blueprint](readme/screenshot-deploymesh-blueprint-bp_placeable-01.png) |
|:---:|
| [View on BlueprintUE](https://blueprintue.com/blueprint/o5ymzm9r/) |

### Create a DeployMeshes blueprint

- Create a blueprint that merges selected static mesh actor(s) and saves the resulting mesh to the path of the placeholder mesh used in *BP_Placeable*. It may help to define a variable for the *Deploy Mesh Path*

| ![Deploy Mesh Blueprint](readme/screenshot-deploymesh-blueprint-deploy-01.png) |
|:---:|
| [View on BlueprintUE](https://blueprintue.com/blueprint/-e7bwmzm/) |

## In Action

### Texturize + Deploy

<div align="center">
![Deploy and Launch](https://github.com/JustAddRobots/vault/assets/59129905/5fb9454e-fa8b-42fa-8d24-13cc0be6d94e)
</div>

### Run

<div align="center">
![Run](https://github.com/JustAddRobots/vault/assets/59129905/e4b24b10-541b-4ecf-afc4-9c1d6c2e8456)
</div>

## TODO

- [ ] Add Scale Divisor as parameter
- [ ] Enable scale manipulation in app
- [ ] Enable real-time scene lighting

## Support

This proof-of-concept is an alpha, thus currently unsupported.
